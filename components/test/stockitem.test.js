import { shallowMount } from '@vue/test-utils'
import { mockResponse } from './search.test'
import StockItem from '@/components/StockItem.vue'

let wrapper

beforeEach(() => {
  wrapper = shallowMount(StockItem, {
    propsData: mockResponse
  })
})

describe('Stock item component', () => {
  test('is a vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
  test('ui match props', () => {
    expect(wrapper.text()).toContain(`$${mockResponse.symbol}`)
    expect(wrapper.text()).toContain(mockResponse.name)
    expect(wrapper.text()).toContain(mockResponse.exchange)
    expect(wrapper.text()).toContain('-0.02%')
  })
})
