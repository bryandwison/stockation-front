import { mockResponse } from '../components/test/search.test'

export default {
  get: () =>
    Promise.resolve({
      data: {
        data: [mockResponse]
      }
    })
}
